# IMU DATA

## Euskaraz
Biltegi honetan IMU_raw izeneko mezuak bidali ahal izateko aukera ematen duen ROS pakete berri bat dago. Honen bidez azelerazioaren, giroskopiaren eta magnetometroaren datu gordinak bidaltzea ahalbidetzen da.

## English
This repository holds a new ROS package to add support for a message called
Imu_raw which is used to send over ROS the raw data of acceleration, gyro and
magnetometer.
